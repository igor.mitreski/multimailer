﻿CREATE TABLE [dbo].[Receivers]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT (newsequentialid()), 
    [Username] NVARCHAR(MAX) NULL, 
    [Name] NVARCHAR(MAX) NULL, 
    [Surname] NVARCHAR(MAX) NULL, 
    [Email] NVARCHAR(MAX) NOT NULL, 
    [RegionId] INT NOT NULL FOREIGN KEY REFERENCES Regions(Id), 
    [IpAddress] NVARCHAR(MAX) NULL, 
    [Active] BIT NOT NULL, 
    [CreatedOn] DATETIME NULL
)

﻿CREATE TABLE [dbo].[Senders]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(150) NULL, 
    [Surname] NVARCHAR(150) NULL, 
    [Email] NVARCHAR(150) NOT NULL, 
    [ProviderId] int NOT NULL FOREIGN KEY REFERENCES Providers(Id), 
    [Password] NVARCHAR(150) NULL
)

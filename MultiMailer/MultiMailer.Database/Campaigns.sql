﻿CREATE TABLE [dbo].[Campaigns]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT (newsequentialid()), 
    [CampaignName] NVARCHAR(150) NOT NULL, 
    [CampaignDescription] NVARCHAR(500) NULL,
    [Html] NVARCHAR(MAX) NULL,
    [HtmlFile] NVARCHAR(150) NULL,
    [Active] BIT NOT NULL 
)

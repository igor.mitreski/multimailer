﻿CREATE TABLE [dbo].[Providers]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(150) NOT NULL, 
    [Smtp] NVARCHAR(150) NOT NULL,
    [SmtpUsername] NVARCHAR(150) NOT NULL,
    [SmtpPassword] NVARCHAR(150) NOT NULL,
    [RequireSsl] BIT NOT NULL, 
    [RequireTls] BIT NULL, 
    [SslPort] INT NULL, 
    [TlsPort] INT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [Active] BIT NOT NULL, 
    [DailyLimit] INT NOT NULL DEFAULT 300, 
    [MonthlyLimit] INT NOT NULL DEFAULT 12000, 
    [DefaultPort] INT NOT NULL DEFAULT 25
)

using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Hangfire;
using Hangfire.MemoryStorage;
using MultiMailer.Repository;
using MultiMailer.Services.Interfaces;
using MultiMailer.Services;
using Hangfire.Dashboard;
using System.Collections.Generic;
using HangfireBasicAuthenticationFilter;

try
{
    var builder = WebApplication.CreateBuilder(args);

    IConfiguration configuration = builder.Configuration;

    #region Configure services

    builder.Services.AddRazorPages();
    builder.Services.AddDbContext<MultiMailerDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));

    builder.Services.AddHangfire(x => x.UseMemoryStorage());
    builder.Services.AddHangfireServer();

    builder.Services.AddScoped<IImportService, ImportService>();
    builder.Services.AddTransient<IMailerService, MailerService>();



    #endregion

    #region Use app

    var app = builder.Build();

    app.UseDeveloperExceptionPage();

    app.UseExceptionHandler(exceptionHandlerApp =>
    {
        exceptionHandlerApp.Run(async context =>
        {
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            context.Response.ContentType = "application/json";

            var exceptionHandlerPathFeature =
                context.Features.Get<IExceptionHandlerPathFeature>();
        });
    });

    app.UseHttpsRedirection();
    var options = new DashboardOptions
    {
        Authorization = new[]
        {
            new HangfireCustomBasicAuthenticationFilter{
                User = "igormitreski",
                Pass = "P@$$w0rd"
            }
        }
    };
    app.UseHangfireDashboard("/hangfire", options);
    var serviceScope = app.Services.CreateScope();
    var services = serviceScope.ServiceProvider;
    var mailerService = services.GetRequiredService<IMailerService>();

    var cronExpression = builder.Configuration.GetSection("GeneralSettings:CronExpression").Value;
    RecurringJob.AddOrUpdate("SendEmail", () => mailerService.SendEmail(), cronExpression);

    
    app.UseStaticFiles(); // added (method should be called before the UseRouting())

    app.UseForwardedHeaders(new ForwardedHeadersOptions
    {
        ForwardedHeaders = ForwardedHeaders.All
    });

    app.UsePathBase(new PathString("/api"));

    app.UseRouting(); // added  (method should be called before the UseAuthorization())

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.Run();

    #endregion
}
catch (Exception ex)
{
    throw;
}
finally
{

}

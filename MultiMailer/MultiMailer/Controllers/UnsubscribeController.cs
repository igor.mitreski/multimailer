﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MultiMailer.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace MultiMailer.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnsubscribeController : Controller
    {

        private readonly IMailerService _mailerService;
        private readonly IWebHostEnvironment _env;

        public UnsubscribeController(IMailerService mailerService)
        {
            _mailerService = mailerService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(Guid subscriptionId)
        {
            try
            {
                await _mailerService.UnsubscribeFromCampaign(subscriptionId);
                return Content("Successfully unsubscribed from mail list.");
            }
            catch(Exception e)
            {
                return Content("Error. " + e.Message);
            }
            
        }
    }
}

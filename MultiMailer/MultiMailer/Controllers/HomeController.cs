﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MultiMailer.Services.Interfaces;
using System;
using System.Text;
using System.Threading.Tasks;

namespace MultiMailer.Web.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IImportService _importService;
        private readonly IMailerService _mailerService;
        private readonly IWebHostEnvironment _env;

        public HomeController(
            IImportService importService, 
            IMailerService mailerService,
            IWebHostEnvironment env)
        {
            _importService = importService;
            _mailerService = mailerService;
            _env = env;
        }

        [Route("importandsubscribe")]
        [HttpGet]
        public async Task<ActionResult> ImportAndSubscribe()
        {
            try 
            { 
                await _importService.ImportAndSubscribe(@"D:\OneDrive\emails\regulatorna3.csv", 1, "Понуда за соработка");
                return Content("Successfully imported and subscribed.");
            }
            catch(Exception e)
            {
                return Content(e.ToString());
            }
        }

        //[Route("import")]
        //[HttpGet]
        //public async Task<ActionResult> Import()
        //{
        //    try
        //    {
        //        await _importService.ImportFromCsv(@"E:\emails1.csv", 1, "Free Bitcoin Campaign");
        //        return Content("Successfully imported and subscribed.");
        //    }
        //    catch (Exception e)
        //    {
        //        return Content(e.ToString());
        //    }
        //}

        [Route("subscribe")]
        [HttpGet]
        public async Task<ActionResult> Subscribe()
        {
            try
            {
                await _importService.SubscribeToCampaign(1, "Понуда за соработка");
                
                return Content("Successfully subscribed.");
            }
            catch (Exception e)
            {
                return Content(e.ToString());
            }
        }

        [Route("subscribelocal")]
        [HttpGet]
        public async Task<ActionResult> SubscribeLocal()
        {
            try
            {
                await _importService.SubscribeLocal(1, "Понуда за соработка");

                return Content("Successfully subscribed.");
            }
            catch (Exception e)
            {
                return Content(e.ToString());
            }
        }



        [Route("startsending")]
        [HttpGet]
        public async Task<ActionResult> StartSending()
        {
            await _mailerService.SendEmail();
            return null;
        }

        [Route("testtemplate")]
        [HttpGet]
        public ActionResult TestTemplate()
        {
            var template = System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "templates\\Ponuda.html"));
            
            
            return Content(template, "application/html", Encoding.Unicode);
        }

        [Route("sendmail")]
        [HttpGet]
        public async Task<ActionResult> SendTestMail()
        {
            var template = System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "templates\\Ponuda.html"), Encoding.UTF8);
            try 
            {
                await _mailerService.SendMailAsync(
                    "Зоран Митрески",
                    "zoran.mitreski@efixelectrical.com.mk",
                    "Macedonia132!",
                    "zoran.mitreski@efixelectrical.com.mk",
                    "webmail.efixelectrical.com.mk",
                    "mitreski2000@yahoo.com",
                    "Понуда за соработка",
                    template,
                    587);
            }
            catch(Exception e)
            {
                return Content(e.ToString());
            }

            return Content(template);
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultiMailer.Models
{
    [Table("Senders")]
    public class Sender
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
        public int ProviderId { get; set; }

        [ForeignKey("ProviderId")]
        public virtual Provider Providers { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace MultiMailer.Models
{
    [Table("EmailTransactions")]
    public class EmailTransaction
    {
        public EmailTransaction() { }

        public EmailTransaction(
            Receiver receiver, 
            Campaign campaign, 
            Provider provider, 
            Sender sender, 
            TransactionStatus status,
            string description) 
        {
            CampaignId = campaign.Id;
            ReceiverId = receiver.Id;
            RegionId = receiver.RegionId;
            SenderId = sender.Id;
            ProviderId = provider.Id;
            Time = DateTime.Now;
            TransactionStatus = status;
            TransactionDescription = description;
        }

        [Key]
        public long Id { get; set; }
        public Guid CampaignId { get; set; }
        public Guid ReceiverId { get; set; }
        public int RegionId { get; set; }
        public int SenderId { get; set; }
        public int ProviderId { get; set; }
        public DateTime Time { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public string TransactionDescription { get; set; }

        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }

        [ForeignKey("ReceiverId")]
        public virtual Receiver Receiver { get; set; }

        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }

        [ForeignKey("SenderId")]
        public virtual Sender Sender { get; set; }

        [ForeignKey("ProviderId")]
        public virtual Provider Provider { get; set; }
    }
}

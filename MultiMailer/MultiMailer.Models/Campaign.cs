﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultiMailer.Models
{
    [Table("Campaigns")]
    public class Campaign
    {
        [Key]
        public Guid Id { get; set; }

        public string CampaignName { get; set; }

        public string CampaignDescription { get; set; }

        public string Html { get; set; }

        public string HtmlFile { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<Receiver> Receivers { get; set; }
    }
}

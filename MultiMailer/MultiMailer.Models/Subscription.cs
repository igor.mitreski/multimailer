﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultiMailer.Models
{
    [Table("Subscriptions")]
    public class Subscription
    {
        [Key]
        public Guid Id { get; set; }

        public Guid ReceiverId { get; set; }

        public Guid CampaignId { get; set; }

        public bool Subscribed { get; set; }

        [ForeignKey("ReceiverId")]
        public virtual Receiver Receiver { get; set; }

        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign{ get; set; }
    }
}

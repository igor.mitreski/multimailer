﻿namespace MultiMailer.Models
{
    public enum TransactionStatus : int
    {
        Initialized = 0,
        InProcess = 1,
        Sent = 2,
        NotSent = 3
    }
}

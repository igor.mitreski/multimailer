﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultiMailer.Models
{
    [Table("Receivers")]
    public class Receiver
    {
        [Key]
        public Guid Id { get; set; }

        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public int RegionId { get; set; }
        public string IpAddress { get; set; }
        public bool Active { get; set; }

        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }

        public virtual ICollection<Campaign> Campaigns { get; set; }
    }
}

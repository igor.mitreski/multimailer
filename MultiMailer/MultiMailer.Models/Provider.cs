﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MultiMailer.Models
{
    [Table("Providers")]
    public class Provider
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Smtp { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public bool RequireSsl { get; set; }
        public int? SslPort { get; set; }
        public int? TlsPort { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public int DailyLimit { get; set; }
        public int MonthlyLimit { get; set; }
        public int DefaultPort { get; set; }
    }
}

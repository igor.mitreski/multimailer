﻿using Microsoft.EntityFrameworkCore;
using MultiMailer.Models;

namespace MultiMailer.Repository
{
    public class MultiMailerDbContext : DbContext
    {
        public MultiMailerDbContext(DbContextOptions<MultiMailerDbContext> options) : base(options)
        {
            //ChangeTracker.AutoDetectChangesEnabled = false;
            //ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Sender> Senders { get; set; }
        public DbSet<Receiver> Receivers { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<EmailTransaction> EmailTransactions { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
    }
}

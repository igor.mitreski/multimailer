﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MultiMailer.Services.Interfaces;
using MultiMailer.Models;
using MultiMailer.Repository;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultiMailer.Services
{
    public class ImportService : IImportService
    {
        public IConfiguration Configuration;
        private MultiMailerDbContext _db;

        public ImportService(
            IConfiguration configuration,
            MultiMailerDbContext db)
        {
            Configuration = configuration;
            _db = db;
        }

        public async Task ImportAndSubscribe(string csvPath, int region, string campaignName)
        {
            var receivers = await ImportFromCsvComma(csvPath, region);
            //var receivers = await _db.Receivers.Where(x => x.RegionId == 1).ToListAsync();
            await SubscribeToCampaign(receivers, campaignName);
        }

        public async Task<List<Receiver>> ImportFromCsv(string csvPath, int region)
        {
            List<Receiver> receiverList = new List<Receiver>();
            using (var reader = new StreamReader(csvPath))
            {
                var count = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    var receiver = new Receiver();
                    var email = values[0].Trim();
                    receiver.Email = email;
                    if (values.Length > 1 && !string.IsNullOrEmpty(values[1]))
                        receiver.Username = values[1];
                    if (values.Length > 2 && !string.IsNullOrEmpty(values[2]))
                        receiver.Name = values[2];
                    if (values.Length > 3 && !string.IsNullOrEmpty(values[3]))
                        receiver.Surname = values[3];

                    receiver.Active = true;
                    receiver.RegionId = region;
                    receiverList.Add(receiver);
                    count++;
                }
                await _db.Receivers.BulkInsertAsync(receiverList, options => {
                    options.InsertIfNotExists = true;
                    options.ColumnPrimaryKeyExpression = c => c.Email;
                });
            }

            return receiverList;
        }

        public async Task<List<Receiver>> ImportFromCsvComma(string csvPath, int region)
        {
            List<Receiver> receiverList = new List<Receiver>();
            using (var reader = new StreamReader(csvPath))
            {
                var count = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    var receiver = new Receiver();
                    var email = values[0].Trim();
                    receiver.Email = email;
                    if (values.Length > 1 && !string.IsNullOrEmpty(values[1]))
                        receiver.Name = values[1];
                        //receiver.Username = values[1];
                    if (values.Length > 10 && !string.IsNullOrEmpty(values[11]))
                        receiver.Name = values[11];
                    if (values.Length > 11 && !string.IsNullOrEmpty(values[12]))
                        receiver.Surname = values[12];

                    receiver.Active = true;
                    receiver.RegionId = region;
                    receiverList.Add(receiver);
                    count++;
                    
                }
                _db.Receivers.AddRange(receiverList);
                await _db.SaveChangesAsync();
            }

            return receiverList;
        }

        public async Task SubscribeToCampaign(List<Receiver> receivers, string campaignName)
        {
            var campaign = await _db.Campaigns.FirstOrDefaultAsync(x => x.CampaignName == campaignName);
            var subscriptionList = new List<Subscription>();
            foreach(var receiver in receivers)
            {
                var subscription = new Subscription();
                subscription.CampaignId = campaign.Id;
                subscription.ReceiverId = receiver.Id;
                subscriptionList.Add(subscription);
            }
            _db.Subscriptions.AddRange(subscriptionList);
            await _db.SaveChangesAsync();
        }

        public async Task SubscribeToCampaign(int regionId, string campaignName)
        {
            var campaign = await _db.Campaigns.FirstOrDefaultAsync(x => x.CampaignName == campaignName);
            var receivers = await _db.Receivers.Where(x => x.RegionId == regionId).ToListAsync();
            foreach (var receiver in receivers)
            {
                var existingSubscription = await _db.Subscriptions.FirstOrDefaultAsync(x => x.CampaignId == campaign.Id && x.ReceiverId == receiver.Id);
                if (existingSubscription == null)
                {
                    var subscription = new Subscription();
                    subscription.CampaignId = campaign.Id;
                    subscription.ReceiverId = receiver.Id;
                    _db.Subscriptions.Add(subscription);
                    await _db.SaveChangesAsync();
                }
            }
        }

        public async Task SubscribeLocal(int regionId, string campaignName)
        {
            var campaign = await _db.Campaigns.FirstOrDefaultAsync(x => x.CampaignName == campaignName);
            var receivers = await _db.Receivers.Where(x => x.RegionId == regionId &&
            x.Email=="igormitreski@yahoo.co.uk" ||
            x.Email=="igor.mitres@gmail.com" ||
            x.Email=="igor.mitreski@outlook.com"
            ).ToListAsync();
            foreach (var receiver in receivers)
            {
                var existingSubscription = await _db.Subscriptions.FirstOrDefaultAsync(x => x.CampaignId == campaign.Id && x.ReceiverId == receiver.Id);
                if (existingSubscription != null)
                {
                    existingSubscription.Subscribed = true;
                    _db.Subscriptions.Update(existingSubscription);
                    await _db.SaveChangesAsync();
                }
            }
        }
    }
}

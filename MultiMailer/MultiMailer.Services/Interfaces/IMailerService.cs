﻿using System;
using System.Threading.Tasks;

namespace MultiMailer.Services.Interfaces
{
	public interface IMailerService
	{
		Task SendEmail();
        Task SendMailAsync(string name,
                           string apiKeyName,
                           string password,
                           string fromEmail,
                           string smtp,
                           string toEmail,
                           string subject,
                           string body,
                           int port,
                           bool? requireSsl = null);

        Task UnsubscribeFromCampaign(Guid subscriptionId);
    }
}

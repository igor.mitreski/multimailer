﻿using MultiMailer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultiMailer.Services.Interfaces
{
	public interface IImportService
	{
		Task ImportAndSubscribe(string csvPath, int region, string campaignName);
		Task<List<Receiver>> ImportFromCsv(string csvPath, int region);
		Task SubscribeToCampaign(List<Receiver> receivers, string campaignName);
		Task SubscribeToCampaign(int regionId, string campaignName);
		Task SubscribeLocal(int regionId, string campaignName);
	}
}

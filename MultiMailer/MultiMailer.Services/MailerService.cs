﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MultiMailer.Services.Interfaces;
using MultiMailer.Repository;
using MultiMailer.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using System.Text;

namespace MultiMailer.Services
{
    public class MailerService : IMailerService
    {
        public IConfiguration Configuration;
        private MultiMailerDbContext _db;
        private IHostingEnvironment _env;
        private readonly string _domain;

        public MailerService(
            IConfiguration configuration,
            MultiMailerDbContext db,
            IHostingEnvironment env)
        {
            _db = db;
            _env = env;
            Configuration = configuration;
            _domain = Configuration.GetSection("GeneralSettings:Domain").Value;
        }

        public async Task SendEmail()
        {
            var campaigns = await _db.Campaigns.Where(x=>x.Active).ToListAsync();
            foreach (var campaign in campaigns)
            {
                var receiver = await FindNotSentMail(campaign.Id);
                var ap = await FindAvailableProvider();
                if (ap == null || receiver == null)
                {
                    throw new Exception("Available provider was not found or limit exceeded.");
                }
                var sender = await _db.Senders.FirstOrDefaultAsync(x => x.ProviderId == ap.Id);
                var template = await MakeTemplate(receiver.Id, campaign.Id);
                    
                try
                {
                    
                    await SendMailAsync(sender.Name + " " + sender.Surname, 
                                        ap.SmtpUsername,
                                        ap.SmtpPassword,
                                        sender.Email, 
                                        ap.Smtp, 
                                        receiver.Email, 
                                        campaign.CampaignDescription, 
                                        template, 
                                        ap.RequireSsl ? ap.SslPort.Value : ap.DefaultPort, ap.RequireSsl);

                    await CreateEmailTransaction(receiver, campaign, ap, sender, TransactionStatus.Sent, "Sent");
                }
                catch (Exception e)
                {
                    await CreateEmailTransaction(receiver, campaign, ap, sender, TransactionStatus.NotSent, e.Message);
                }
            }
        }

        public async Task SendMailAsync(string name,
                                   string apiKeyName,
                                   string password,
                                   string fromEmail,
                                   string smtp,
                                   string toEmail,
                                   string subject,
                                   string body,
                                   int port,
                                   bool? requireSsl = null)
        {
            var client = new SmtpClient(smtp, port);
            if(requireSsl.HasValue)
                client.EnableSsl = requireSsl.Value;
            var credentials = new NetworkCredential(apiKeyName, password);
            client.Credentials = credentials;

            var msg = new MailMessage();
            msg.From = new MailAddress(fromEmail, name);
            msg.To.Add(new MailAddress(toEmail));
            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;

            await client.SendMailAsync(msg);
        }

        public async Task UnsubscribeFromCampaign(Guid subscriptionId)
        {
            var subscription = await _db.Subscriptions.FindAsync(subscriptionId);
            if (subscription != null)
            {
                subscription.Subscribed = false;
                _db.Subscriptions.Update(subscription);
                await _db.SaveChangesAsync();
            }
            else throw new Exception("Wrong subscription");
        }

        private async Task<string> MakeTemplate(Guid receiverId, Guid campaignId)
        {
            var subscription = await _db.Subscriptions.FirstOrDefaultAsync(x => x.ReceiverId == receiverId && x.CampaignId == campaignId);
            var template = System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "templates\\Ponuda.html"));
            var unsubscribeLink = string.Format("{0}/unsubscribe?subscriptionId={1}", _domain, subscription.Id);
            template = template.Replace("{UNSUBSCRIBE_LINK}", unsubscribeLink);

            return template;
        }

        private async Task CreateEmailTransaction(Receiver receiver, Campaign campaign, Provider provider, Sender sender, TransactionStatus status, string desc = null)
        {
            var emailTransaction = new EmailTransaction(receiver, campaign, provider, sender, status, desc);
            _db.EmailTransactions.Add(emailTransaction);
            await _db.SaveChangesAsync();
        }

        private async Task<Receiver> FindNotSentMail(Guid campaignId)
        {
            var subscribedEmails = await _db.Subscriptions.Where(x => x.CampaignId == campaignId && x.Subscribed).Select(x => x.ReceiverId).ToListAsync();
            var alreadySentForCampaign = await _db.EmailTransactions.Where(x => x.CampaignId==campaignId && x.TransactionStatus == TransactionStatus.Sent).Select(x => x.ReceiverId).ToListAsync();
            var nextSubscribedMailId = subscribedEmails.Except(alreadySentForCampaign).FirstOrDefault();
            var nextReceiver = await _db.Receivers.FindAsync(nextSubscribedMailId);

            return nextReceiver;
        }

        private async Task<Provider> FindAvailableProvider()
        {
            var providers = await _db.Providers.Where(x=>x.Active).ToListAsync();
            var availableProviders = new List<Provider>();
            foreach(var provider in providers)
            {
                var date = DateTime.Now.Date;
                var month = DateTime.Now.Month;
                var dailyEt = await _db.EmailTransactions.Where(x => x.ProviderId == provider.Id
                                                            && x.Time.Date==date).CountAsync();
                var monthlyEt = await _db.EmailTransactions.Where(x => x.ProviderId == provider.Id
                                                            && x.Time.Month == month).CountAsync();

                if(provider.DailyLimit >= dailyEt && provider.MonthlyLimit >= monthlyEt)
                {
                    return provider;
                }
            }

            return availableProviders.FirstOrDefault();
        }
    }
}